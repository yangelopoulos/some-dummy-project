package com.yangelopoulos.testSonar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSonarApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSonarApplication.class, args);
	}

	public void hello(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j){
		System.out.println("Hello hellooooo");
	}

	public void anotherBadMethod(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j){
		System.out.println("Hello hellooooo");
	}

}
